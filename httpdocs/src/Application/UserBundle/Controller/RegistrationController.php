<?php

namespace Application\UserBundle\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\RememberMe\TokenBasedRememberMeServices;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class RegistrationController
 * @package Application\UserBundle\Controller
 * @author Sergey Shaklein
 */
class RegistrationController extends BaseController
{
    /**
     * Receive the confirmation token from user email provider, login the user
     *
     * @param Request $request
     * @param $token
     * @return null|RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     */
    public function confirmAction(Request $request, $token)
    {
        $user = $this->container->get('fos_user.user_manager')->findUserByConfirmationToken($token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
        }


        $user->setConfirmationToken(null);
        $user->setEnabled(true);

        $user->setLastLogin(new \DateTime());

        $this->container->get('fos_user.user_manager')->updateUser($user);

        //new way to auth users
        $providerKey = $this->container->getParameter('fos_user.firewall_name');

        $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());


        $this->container->get('security.context')->setToken($token);

        //set remember me cookie

        $rememberMeService = new TokenBasedRememberMeServices(array($user), 'secret-key-Hjg$hghb655', 'main', array(
                'path' => '/',
                'name' => 'REMEMBERME',
                'domain' => null,
                'secure' => false,
                'httponly' => true,
                'lifetime' => 31536000, // 14 days
                'always_remember_me' => true,
                'remember_me_parameter' => '_remember_me')
        );
        $response = new RedirectResponse($this->container->get('router')->generate('homepage'));
        $rememberMeService->loginSuccess($this->container->get('request'), $response, $this->container->get('security.context')->getToken());

        return  $response;
    }


}
