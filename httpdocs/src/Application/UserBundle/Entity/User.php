<?php

namespace Application\UserBundle\Entity;


use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 * @author Sergey Shaklein
 * @package Application\UserBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * @var string
     * @Assert\NotBlank(message="Please enter email", groups={"Registration", "Profile"})
     */
    protected $email;
    /**
     * @var string
     */
    protected $emailCanonical;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="You don’t enter the First Name", groups={"Registration", "Profile"})
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="You don’t enter Last Name", groups={"Registration", "Profile"})
     */
    protected $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Enter the birth date", groups={"Registration", "Profile"})
     */
    protected $birthdate;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Choose the marital status", groups={"Registration", "Profile"})
     */
    protected $martial_status;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Choose the body type", groups={"Registration", "Profile"})
     */
    protected $body_type;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Choose the sex", groups={"Registration", "Profile"})
     */
    protected $sex;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $breast;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $waist;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $hips;


    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="You don’t enter City", groups={"Registration", "Profile"})
     */
    protected $city;


    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="You don’t enter Country", groups={"Registration", "Profile"})
     */
    protected $country;


    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * @param $value
     */
    public function setBirthdate($value)
    {
        $this->birthdate = $value;
    }

    /**
     * @return string
     */
    public function getBodyType()
    {
        return $this->body_type;
    }

    /**
     * @param $value
     */
    public function setBodyType($value)
    {
        $this->body_type = $value;
    }

    /**
     * @return string
     */
    public function getBreast()
    {
        return $this->breast;
    }

    /**
     * @param $value
     */
    public function setBreast($value)
    {
        $this->breast = $value;
    }

    /**
     * @return string
     */
    public function getWaist()
    {
        return $this->waist;
    }

    /**
     * @param $value
     */
    public function setWaist($value)
    {
        $this->waist = $value;
    }

    /**
     * @return string
     */
    public function getHips()
    {
        return $this->hips;
    }

    /**
     * @param $value
     */
    public function setHips($value)
    {
        $this->hips = $value;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param $value
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getMartialStatus()
    {
        return $this->martial_status;
    }

    /**
     * @param $value
     */
    public function setMartialStatus($value)
    {
        $this->martial_status = $value;
    }

    /**
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param $value
     */
    public function setSex($value)
    {
        $this->sex = $value;
    }


    /**
     * @param $value
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }


    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        if (is_null($this->getUsername())) {
            $this->setUsername($email);
        }

        return parent::setEmail($email);
    }


}