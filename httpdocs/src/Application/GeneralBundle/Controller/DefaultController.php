<?php

namespace Application\GeneralBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class DefaultController
 * @author Sergey Shaklein
 * @package Application\GeneralBundle\Controller
 *
 */
class DefaultController extends Controller
{
    /**
     * Home page action. Do nothing but shows home page template
     * @Route("/", name="homepage")
     *
     */
    public function indexAction()
    {
        return $this->render('ApplicationGeneralBundle:Default:index.html.twig');

    }
}
